package com.example.doux.androidstudioexercices;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Exercice030_2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercice030_2);

        /*Intent t = getIntent();
        String prenomRecup;
        prenomRecup = t.getStringExtra("Prenom");*/

        Intent intent = getIntent();
        String name = intent.getStringExtra("name");

        TextView affichage = findViewById(R.id.prenomSaisiDisplay);

        affichage.setText("Bonjour " + name);
    }

}
