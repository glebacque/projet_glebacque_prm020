package com.example.doux.androidstudioexercices;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Exercice030Activity extends AppCompatActivity {

    EditText prenomSaisi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercice030);
    }

    public void clicSurSuivant(View v){
        prenomSaisi = findViewById(R.id.sai_prenom);
        String textToJoin = prenomSaisi.getText().toString();
        Intent intent = new Intent(this,Exercice030_2Activity.class);
        intent.putExtra("name",textToJoin);
        startActivity(intent);
    }


}
