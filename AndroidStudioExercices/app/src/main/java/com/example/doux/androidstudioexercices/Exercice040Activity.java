package com.example.doux.androidstudioexercices;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Exercice040Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercice040);
    }

    public void calCul()
    {
        EditText op1 = findViewById(R.id.firstop);
        EditText op2 = findViewById(R.id.secondop);
        String strOp1 = op1.getText().toString();
        String strOp2 = op2.getText().toString();
        long itOp1 = Long.valueOf(strOp1);
        long itOp2 = Long.valueOf(strOp2);
        long resultat = itOp1 + itOp2;
        String resFinal = String.valueOf(resultat);
        TextView affichage = findViewById(R.id.resultatDisplay);
        affichage.setText(resFinal);
    }
}
